const JSLoader = {
  test: /\.js$/,
  exclude: /node_modules/,
  use: [{
    loader: 'babel-loader',
    options: {
      presets: ['@babel/preset-env', { plugins: ['@babel/plugin-proposal-class-properties'] }]
    }
  }, 'source-map-loader']
};
module.exports = {
  JSLoader: JSLoader // eslint-disable-line object-shorthand
};

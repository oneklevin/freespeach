import React from 'react';
import { Login } from './login';
import { Register } from './register';
import styles from './join-us.less';

export const JoinUs = () => {
  return (
    <div className={styles.JoinUs}>
      <Login />
      <Register />
    </div>
  );
};

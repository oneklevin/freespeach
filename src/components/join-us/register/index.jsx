import React, { useState } from 'react';
import styles from './register.less';

const Register = () => {
  const [registerDetails, setRegisterDetails] = useState({
    username: '',
    email: '',
    password: '',
    passwordRepeated: '',
  });

  const onChangeValue = ({ target: { name, value } }) => {
    setRegisterDetails({ ...registerDetails, [name]: value });
  };

  return (
    <div className={styles.Register}>
      <p>Regjistrohu</p>
      <label>Username:</label>
      <input
        type="text"
        name="username"
        placeholder='Ju lutemi shkruani username-in'
        onChange={onChangeValue}
        value={registerDetails.username}
      />
      <label>Email:</label>
      <input
        type="text"
        name="email"
        placeholder='Ju lutemi shkrani email-in'
        onChange={onChangeValue}
        value={registerDetails.email}
      />
      <label>Passwordi:</label>
      <input
        type="password"
        name="password"
        placeholder='Ju lutemi shkruani password-in'
        onChange={onChangeValue}
        value={registerDetails.password}
      />
      <label>Perserit password-in:</label>
      <input
        type="password"
        name="passwordRepeated"
        placeholder='Ju lutemi ri-shkruani password-in'
        onChange={onChangeValue}
        value={registerDetails.passwordRepeated}
      />
      <button>Regjistrohu</button>
    </div>
  );
};

export { Register as Register };

import React, { useState } from 'react';
import styles from './login.less';

const Login = () => {
  const [loginDetails, setLoginDetails] = useState({ email: '', password: '' });

  const onChangeValue = ({ target: { name, value } }) => {
    setLoginDetails({ ...loginDetails, [name]: value });
  };

  return (
    <div className={styles.Login}>
      <p>Login</p>
      <label>Email:</label>
      <input
        type="text"
        name="email"
        placeholder='Ju lutemi shkruani email-in'
        onChange={onChangeValue}
        value={loginDetails.email}
      />
      <label>Password:</label>
      <input
        type="password"
        name="password"
        placeholder='Ju lutemi shkruani password-in'
        onChange={onChangeValue}
        value={loginDetails.password}
      />
      <button onClick={() => console.log('login')}>Login</button>
    </div>
  );
};

export { Login as Login };

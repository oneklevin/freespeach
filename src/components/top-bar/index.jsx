import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import styles from './top-bar.less';

const TopBar = ({
  userstore,
}) => {
  const { isUserLoggedIn } = userstore;
  return (
    <div className={styles.TopBar}>
      <Link to="/" className={styles.Logo}>Mendim I Lirë</Link>
      <ul>
        {!isUserLoggedIn && (
          <>
            <li><Link to="/">Kryesore</Link></li>
            <li><Link to="/rreth-nesh">Rreth nesh</Link></li>
            <li><Link to="/bashkohuni">Bashkohuni</Link></li>
          </>
        )}
        {isUserLoggedIn && (
          <>
            <li><Link to="/postimet">Postimet</Link></li>
            <li><Link to="/profili-im">Profili im</Link></li>
            <li>
              <img className={styles.UsersProfilePhoto} src="https://images.unsplash.com/photo-1453728013993-6d66e9c9123a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8bGVuc3xlbnwwfHwwfHw%3D&w=1000&q=80" />
            </li>
          </>
        )}
      </ul>
    </div>
  );
};

TopBar.propTypes = {
  userstore: PropTypes.object.isRequired,
};

const mapStateToProps = ({ userstore }) => ({ userstore });

const Container = connect(mapStateToProps)(TopBar);
export { Container as TopBar };


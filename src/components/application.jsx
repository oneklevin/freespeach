
import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import PropTypes from 'prop-types';
import { TopBar } from './top-bar';
import { MainView } from './main-view';
import { LandingPage } from './landing-page';
import { Footer } from './footer';
import { JoinUs } from './join-us';
import './application.less';

const Application = ({
  isUserLoggedIn,
}) => {
  return (
    <>
      <Router>
        <TopBar />
        <Routes>
          {isUserLoggedIn
            ? <Route path="/" element={<MainView />} />
            : <Route path="/" element={<LandingPage />} />
          }
          <Route path="/bashkohuni" element={<JoinUs />} />
        </Routes>
        <Footer />
      </Router>
    </>
  );
};

Application.propTypes = {
  isUserLoggedIn: PropTypes.bool.isRequired,
};

const mapStateToProps = ({ userstore }) => ({
  isUserLoggedIn: userstore.isUserLoggedIn,
});

const Container = connect(mapStateToProps)(Application);
export { Container as Application };


import React from 'react';
import { LeftSideContent } from '../side-content/left-side';
import { RightSideContent } from '../side-content/right-side';
import { Posts } from './posts';
import styles from './main-view.less';

export const MainView = () => {
  return (
    <div className={styles.MainView}>
      <LeftSideContent />
      <Posts />
      <RightSideContent />
    </div>
  );
};

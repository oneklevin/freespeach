import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import userstore from './user';

const reducer = combineReducers({
  userstore,
});
const store = configureStore({
  reducer,
});
export default store;
